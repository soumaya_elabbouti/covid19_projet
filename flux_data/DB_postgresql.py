#!/usr/bin/env python
# coding: utf-8




# charger les packages
from sqlalchemy import create_engine
import pandas as pd
import requests 
import ssl




# autoriser l'accès a des sites internet
ssl._create_default_https_context = ssl._create_unverified_context
# se connecter à la base données postgresql grâce aux identifiants
postgresql = create_engine('postgresql://postgres:soumaya@localhost:5432/covid19_projet')




# données concernant les cas covid19 dans le monde
raw = requests.get("https://services1.arcgis.com/0MSEUqKaxRlEPj5g/arcgis/rest/services/Coronavirus_2019_nCoV_Cases/FeatureServer/2/query?where=1%3D1&outFields=*&outSR=4326&f=json")
raw_json = raw.json()
df_world = pd.DataFrame(raw_json["features"])
df_world = df_world["attributes"].tolist()
df_final_world = pd.DataFrame(df_world)
df_final_world = df_final_world[["Country_Region", "Lat", "Long_", "Confirmed", "Deaths", "Recovered"]]

 # créer une table dans la base de données covid19 sur postgresql et mette à jour
df_final_world.to_sql('world_data', postgresql, if_exists='replace')




# données concernant le dépistage par département
depist = pd.read_csv("https://www.data.gouv.fr/fr/datasets/r/b4ea7b4b-b7d1-4885-a099-71852291ff20", sep = ";")
depist = depist.loc[depist['clage_covid'] == '0'] # avoir tous les ages compris
depist = depist[["dep","jour","nb_test","nb_pos"]]

#engine.execute("CREATE TABLE IF NOT EXISTS depistage (dep text,jour date,clage_covid text,nb_test integer,nb_pos integer,nb_test_h integer,nb_pos_h integer,nb_test_f integer,nb_pos_f integer)")       
#engine.execute("COPY depistage FROM PROGRAM 'curl https://static.data.gouv.fr/resources/donnees-relatives-aux-tests-de-depistage-de-covid-19-realises-en-laboratoire-de-ville/20200529-190031/donnees-tests-covid19-labo-quotidien-2020-05-29-19h00.csv' CSV HEADER DELIMITER ';' ")   

 # créer une table dans la base de données covid19 sur postgresql et mette à jour
depist.to_sql('depistage', postgresql, if_exists='replace')





# données concernant les cas hospitalisés/réanimés/décès/guéris
sante_public = pd.read_csv("https://www.data.gouv.fr/fr/datasets/r/6fadff46-9efd-4c53-942a-54aca783c30c", sep = ";")
sante_public = sante_public.rename(columns={'incid_hosp':'hosp',"incid_rea":"rea", "incid_rad":"rad",'incid_dc':'dc'})
sante_public = sante_public[["dep","jour","hosp","rea","rad","dc"]]

#engine.execute("CREATE TABLE IF NOT EXISTS hosp_remis_rea_dc (dep text,jour date,indic_hosp text,incid_rea text,incid_dc text,incid_rad text)")
#engine.execute("COPY hosp_remis_rea_dc FROM PROGRAM 'curl https://static.data.gouv.fr/resources/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/20200803-190016/donnees-hospitalieres-nouveaux-covid19-2020-08-03-19h00.csv' CSV HEADER DELIMITER ';' ")    

 # créer une table dans la base de données covid19 sur postgresql et mette à jour
sante_public.to_sql('hosp_remis_rea_dc', postgresql, if_exists='replace')





# données concernant le nombre de cas confirmés par régions
casconf = pd.read_csv('https://raw.githubusercontent.com/opencovid19-fr/data/master/dist/chiffres-cles.csv')
casconf_reg = casconf.loc[casconf['granularite'] == 'region']
casconf_reg = casconf_reg[["date", "maille_nom", "cas_confirmes"]]
casconf_reg = casconf_reg.rename(columns={'maille_nom': 'region_name', "date": "jour"})
casconf_reg = casconf_reg.drop_duplicates(['jour','region_name'], keep='first')
casconf_reg = casconf_reg.dropna()

#engine.execute("CREATE TABLE IF NOT EXISTS nb_cas (date date,granularite text,maille_code text,maille_nom text,cas_confirmes text,cas_ehpad text,cas_confirmes_ehpad text,cas_possibles_ehpad text,deces text,deces_ehpad text,reanimation text,hospitalises text,nouvelles_hospitalisations text,nouvelles_reanimations text,gueris text,depistes text,source_nom text,source_url text,source_archive text,source_type text)")
#engine.execute("COPY nb_cas FROM PROGRAM 'curl https://raw.githubusercontent.com/opencovid19-fr/data/master/dist/chiffres-cles.csv' CSV HEADER")   

 # créer une table dans la base de données covid19 sur postgresql et mette à jour
casconf_reg.to_sql('nb_cas_reg', postgresql, if_exists='replace')






# données concernant le nombre de cas confirmés pour l'ensemble du pays
casconf_fr = pd.read_csv("https://www.coronavirus-statistiques.com/corostats/openstats/open_stats_coronavirus.csv?fbclid=IwAR3UIrk4TZ6B0ZrJGEvKSgo9tULZsQDiIWGMSQF1WwK8bqzUBIWHoEUht7g",sep=";")
casconf_fr = casconf_fr.loc[casconf_fr['nom'] == "france"]
casconf_fr = casconf_fr.drop(['source','code','nom','guerisons','deces'], axis = 1)
casconf_fr = casconf_fr.rename(columns={'cas': 'cas_confirmes'})
casconf_fr['date'] = pd.to_datetime(casconf_fr['date'])
casconf_fr = casconf_fr.fillna(0)

 # créer une table dans la base de données covid19 sur postgresql et mette à jour
casconf_fr.to_sql('nb_cas_fr', postgresql, if_exists='replace')

