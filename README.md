#### Ce projet contient plusieurs dossiers.)
 - Installer les packages requis (voir "dashboard/requirements.txt")
 - Données disponibles dans flux_data/data

## **1. Dashboard  (/dashboard)**
    - Importer les données stockées sur PostgreSQL via les identifiants (voir "flux_data/DB_postgresql.py")
        * Sinon, importer les données depuis le dossier "flux_data/data", un codebook est disponible décrivant les variables des tables de données.
    - Lancer le script "app.py" pour configurer et déployer le tableau de bord.


### Disponible également sur le lien suivant : https://covid19-soumaya.herokuapp.com/. Voici l'aperçu ci-joint : 


![Capture_d_écran_2020-08-31_à_12.57.08](/uploads/7e5a994bc83974389940373a62142f75/Capture_d_écran_2020-08-31_à_12.57.08.png)
![Capture_d_écran_2020-08-31_à_12.59.36](/uploads/f13de434f4622e607a6a167ea1f713c5/Capture_d_écran_2020-08-31_à_12.59.36.png)
![Capture_d_écran_2020-08-31_à_12.59.51](/uploads/2afe5eed22566114e7c62848135f78c4/Capture_d_écran_2020-08-31_à_12.59.51.png)

<!-- blank line -->
----
<!-- blank line -->


## **2. Modèle SIR (/SIR_model)**
    - Lancer le script "sir_model.py" pour créer une fonction de simulation d'un modèle SIR
    - "SIR_model/figures" : graphiques intéractifs du modèle SIR à partir de nos données (/graphique_cas_covid.html) ou par simulation (modele_sir.html) - à télécharger pour l'ouvrir -
![Capture_d_écran_2020-08-31_à_13.22.35](/uploads/d7e7d6c9f6dbf8f856528ceaae349196/Capture_d_écran_2020-08-31_à_13.22.35.png)
![Capture_d_écran_2020-08-31_à_13.22.46](/uploads/4d7ee1623908cca3fc0ec0cb6911104c/Capture_d_écran_2020-08-31_à_13.22.46.png)

<!-- blank line -->
----
<!-- blank line -->

## **3. Google Mobility (/google_mobility)**
    - Lancer le script "scrap_mobility_google.py" : scraper les données et lancer un graphique reprenant les tendances des déplacements en France dans différents lieux (et le nombre cas confirmés).
    - "google_mobility/figures" : graphique intéractif  - à télécharger pour l'ouvrir -
![Capture_d_écran_2020-08-31_à_13.23.19](/uploads/8af9e6b4ef793c807c56663a2b2bc544/Capture_d_écran_2020-08-31_à_13.23.19.png)


