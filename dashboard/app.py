#!/usr/bin/env python
# coding: utf-8

# In[1]:


# charger les packages

from dash.dependencies import Input, Output
from plotly.subplots import make_subplots
from sqlalchemy import create_engine
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import pandas as pd
import requests
#import numpy
#import json
import dash
import csv
import ssl


# se connecter à la base donnees postgresql grâce aux identifiants PostgreSQL
postgresql = create_engine('postgresql://postgres:soumaya@localhost:5432/covid19_projet')
ssl._create_default_https_context = ssl._create_unverified_context



## OUVRIR LES DONNEES DEPUIS LA BASE DE DONNEES POSTGRESQL

## données concernant le covid mondial
df_final_world = pd.read_sql("""
            SELECT *
            FROM world_data
            """, con = postgresql)



## cas confirmés (plusieurs sources : ARS, santé publique, etc) par régions et pour la France
casconf_reg = pd.read_sql("""
            SELECT *
            FROM nb_cas_reg
            """, con = postgresql)
casconf_fr = pd.read_sql("""
            SELECT *
            FROM nb_cas_fr
            """, con = postgresql)



## données concernant les personnes guéries/décédées/hospitalisées/réanimées
sante_public = pd.read_sql("""
            SELECT *
            FROM hosp_remis_rea_dc
            """, con = postgresql)
del sante_public['index']



## données concernant les tests de dépistages par département
depist = pd.read_sql("""
            SELECT *
            FROM depistage
            """, con = postgresql)



## code des départements
code_dep = pd.read_csv('https://www.data.gouv.fr/fr/datasets/r/987227fb-dcb2-429e-96af-8979f97c9c84')
code_dep = code_dep.rename(columns={'num_dep': 'dep'})



# merger les données de la table santé_public + les données concernant les tests de dépistage
df_santepub_dep = pd.merge(sante_public, depist, on=["dep","jour"], how="left")
df_santepub_dep = pd.merge(df_santepub_dep, code_dep, on=["dep"], how="left")




## AGGREGATION DES TABLEES 

# Par département, avoir le total de décés (dc), réanimés (rea), hospotalisés (hosp) et guéris (rad)
df_total = df_santepub_dep.groupby("dep_name", as_index=False).agg(
    {
        "dc" : "sum",
        "rad" : "sum",
        "hosp" : "sum",
        "rea" : "sum"
    }
)

# Avoir le top 10 des départements ayant un taux de décès élevé
df_top10 = df_total.nlargest(10, "dc")
top10_countries = df_top10["dep_name"].tolist()
top10_dc = df_top10["dc"].tolist()

# Avoir le nombre total pour chaque type de cas (cas confirmeé, décès, guéris, réanimés et hospitalisés)
total_confirmed = casconf_fr['cas_confirmes'].iloc[-1]
total_deaths = df_santepub_dep["dc"].sum()
total_recovered = df_santepub_dep["rad"].sum()
total_hosp = df_santepub_dep["hosp"].sum()
total_rea = df_santepub_dep["rea"].sum()

# Avoir les noms uniques des régions
regions = df_santepub_dep['region_name'].unique()




## Construire la grille pour insérer les figures
fig = make_subplots(
    rows = 4, cols = 6,
    specs=[
            [    {"type": "scattergeo", "rowspan": 4, "colspan": 3}, None, None, {"type": "indicator","colspan": 3}, None, None],
            [    None, None, None, {"type": "indicator"},{"type": "indicator"},{"type": "indicator"}],
            [    None, None, None, {"type": "indicator"}, None, {"type": "indicator"}],
            [    None, None, None, {"type": "bar", "colspan":3}, None, None],
          ],
    subplot_titles=(None,"Cas du Covid-19 en France",None,None,None,None,None,"Taux Mortalité : TOP10 des départements"),
    vertical_spacing=0.1
)



## Ajouter une colonne pour les annotations pour afficher les informations (décès, etc) dans la world map
message = df_final_world["Country_Region"] + "<br>"
message += "confirmés: " + df_final_world["Confirmed"].astype(str) + "<br>"
message += "décès: " + df_final_world["Deaths"].astype(str) + "<br>"
message += "guéris: " + df_final_world["Recovered"].astype(str) + "<br>"
df_final_world["text"] = message



## Configuration de la world map 
fig.add_trace(
    go.Scattergeo(
        lon = df_final_world["Long_"],
        lat = df_final_world["Lat"],
        hovertext = df_final_world["text"],
        showlegend=False,
        marker = dict(
            size = 10,
            opacity = 0.8,
            reversescale = True,
            autocolorscale = True,
            symbol = 'square',
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),
            cmin = 0,
            color = df_final_world['Confirmed'],
            cmax = df_final_world['Confirmed'].max(),
            colorbar_title="Cas confirmés",  
            colorbar_x = -0.1
        )

    ),
   
    row=1, col=1
)



## Configuration des indicateurs (nombre de cas confirmés, etc)
fig.add_trace(
    go.Indicator(
        mode="number",
        value=total_confirmed,
        title="Confirmés",
    ),
    row=2, col=4
)

fig.add_trace(
    go.Indicator(
        mode="number",
        value=total_recovered,
        title="Guérisons",
    ),
    row=2, col=5
)

fig.add_trace(
    go.Indicator(
        mode="number",
        value=total_deaths,
        title="Décès",
    ),
    row=2, col=6
)

fig.add_trace(
    go.Indicator(
        mode="number",
        value=total_rea,
        title="Réanimés",
    ),
    row=3, col=4
)

fig.add_trace(
    go.Indicator(
        mode="number",
        value=total_hosp,
        title="Hospitalisés",
    ),
    row=3, col=6
)


## Configuration du graphique en bâton pour avoir le top 10 des départements touchés par un taux de décès élevé
fig.add_trace(
    go.Bar(
        x=top10_countries,
        y=top10_dc,
        marker=dict(color="Red"),
        showlegend=False,
    ),
    row=4, col=4
)

## Configuration des paramètres de visualisation (template, police d'écriture, etc) de notre dashboard
fig.update_layout(
    template="plotly_dark",
    title = "Cas du COVID-19 dans le Monde",
    yaxis=dict(showgrid=False),
    showlegend=True,
    legend_orientation="h",
    legend=dict(x=0.65, y=0.8),
    geo = dict(
            projection_type="orthographic",
            showcoastlines=True,
            landcolor="white",
            showland= True,
            showocean = True,
            lakecolor= "LightBlue"
    )
)



## CREATION DU DASHBOARD

external_stylesheets = [
    "https://codepen.io/chriddyp/pen/bWLwgP.css",
    "/assets/style.css",
]
app = dash.Dash(
    __name__,
    external_stylesheets=external_stylesheets,
    meta_tags=[
        {"name": "viewport", "content": "width=device-width, initial-scale=1.0"}
    ],
)


server = app.server


# configuration des composants du dashboard
app.layout = html.Div([
        dcc.Tabs(id="tabs",
                children=[
                   
            dcc.Tab(label='Aperçu de la pandémie COVID-19 dans le Monde',
                    value='tab-1',
                    children = [
                               dcc.Graph(
                                   style={"height":"100vh"},
                                   figure=fig
                               )]
                    ),
                   
            dcc.Tab(label="Chiffres de la pandémie COVID-19 en France",
                    value='tab-2',
                    children=[
                        
                 html.Div(
                    className="content",
                    children=[
                
                    html.Div(
                    className="left_menu", children=[      
                        html.Div(children=[
                            html.H3('Sélectionner la région :'),
                            dcc.Dropdown(
                                    id='metric-regions',
                                    options=[{'label': i, 'value': i} for i in regions],
                                    value='Hauts-de-France'),
                            html.Div([]),
                            html.H3('Sélectionner la métrique :'),
                            dcc.Checklist(
                                    id='metric-list',
                                    options=[{'label': m, 'value': m} for m in ['Hospitalisés','Décès','Guéris']],
                                    value=['Hospitalisés','Décès','Guéris'])                       
                        ]), 
                    ],
                        style = {
                        'width': '20%',
                        'position': 'fixed',
                        'left': '0',
                        'height': '100vh',
                        'z-index': '999',
                        'background':'#dce6e6',
                        'textAlign': 'center',
                        'color': '#000000',
                        'font-family':'Rockwell', 
                        'font-size':'18'}
                    ),

       
                    html.Div(
                    className="content",
                    children=[
                        html.Div(
                            children=[ 
                                html.P(' '),
                                html.Div(
                                    [html.P("Nombre total de tests de dépistage effectués : "), html.H3(id="test_txt")],
                                    className="five columns",
                                    style =  {'background':'#F7F7F7',
                                              'color':'#000000',
                                              'font-family':'Rockwell',
                                             }
                                ),
                                html.Div(
                                    [html.P("Nombre total de tests de dépistage positifs :"), html.H3(id="testpos_txt")],
                                    className="five columns",
                                    style =  {'background': '#F7F7F7',
                                              'color':'#000000',
                                              'font-family':'Rockwell', 
                                              'font-size':'18'
                                             }
                                ), 
                                ],
                            style = {'width':'75%',
                                    'position':'absolute',
                                    'center': '0',
                                    'right': '0',
                                    'textAlign':'center',
                                    'height': '80vh',
                                    'z-index': '999'}
                        ),
                        
                        ], style = {
                        'background': '#2A3F54',
                        'color':'#dce6e6',
                        'height': '120px',
                        'width':'80',
                        'position':'relative',
                        'top': '0',
                        'right': '0'}),
                
                     
                        html.Div([
                             dcc.Graph(id='indicator-graphic'),
                             dcc.Graph(id='indicator-graphic2')  
                            ],
                             style = {'width':'80%',
                                      'position':'absolute',
                                      'down': '0',
                                      'right': '0',
                                      'textAlign':'center'}),
                        
                    ], style =  {'width': '100%','background': '#F7F7F7'}
                    ),

            ]),
        ],
                colors={
                    "border": "white",
                    "primary": "#dce6e6",
                    "background": "#dce6e6"},
                style =  {'font-family':'Rockwell'},
    ),
])




# configuration des sorties des composantes du dashboard à partir de la fonction @app.callback
@app.callback(
    Output('indicator-graphic', 'figure'),
    [Input('metric-regions', 'value'),Input('metric-list', 'value')])

def update_graph(metric_regions_name, metric_list_name):    
    d = df_santepub_dep
    data = d.loc[d['region_name'] == metric_regions_name].drop(['region_name','dep','dep_name','rea'], axis=1)
    data = data.astype({'jour':'datetime64[ns]'})
    data = data.groupby('jour',as_index=False).sum()
    data = data.sort_values(by=['jour'])
    data['dateStr'] = data['jour'].dt.strftime('%b %d, %Y')
    data = data.rename(columns={'dc': 'Décès', "rad": "Guéris", "hosp":"Hospitalisés"})
    metrics = ['Décès','Guéris','Hospitalisés']

    data = [
        go.Scatter(
            name = metrics,
            x=data.dateStr, y=data[metrics],
            mode = "lines", 
            stackgroup='one',
            line=dict(width=0.5),
            marker_color={ 'Décès':'rgb(200,30,30)', 'Hospitalisés':'rgb(100,140,240)', 'Guéris':'rgb(50,200,200)' }[metrics])
        for metrics in metric_list_name]
   
    layout = go.Layout(
        title="Evolution quotidienne des cas COVID-19",
        height=600,
        yaxis_title="Nombre de personnes",
        legend_title="Choix des métriques",
        yaxis=dict(showgrid=False),
        xaxis=dict(showgrid=False),
        hovermode = "x",
        font=dict(
            family="Arial",
            size=16),
        title_font = dict(family = 'Rockwell', size = 24),
        template = 'plotly_dark'
        )
    
    fig2 = go.Figure(data=data, layout=layout)
    return fig2



@app.callback(
    Output('indicator-graphic2', 'figure'),
    [Input('metric-regions', 'value')])

def update_graph(metric_regions_name):  
    d2 = casconf_reg
    data2 = d2.loc[d2['region_name'] == metric_regions_name]
    data2 = data2.astype({'jour':'datetime64[ns]'})

    data2 = [
        go.Scatter(
            x=data2.jour, y=data2.cas_confirmes,
            mode = "lines", fill='tozeroy')]
   
    layout2 = go.Layout(
        title="Cumul quotidien des cas confirmés COVID-19",
        height=600,
        yaxis_title="Nombre de personnes",
        legend_title="Choix des métriques",
        yaxis=dict(showgrid=False),
        xaxis=dict(showgrid=False),
        hovermode = "closest",
        font=dict(
            family="Arial",
            size=16),
        title_font = dict(family = 'Rockwell', size = 24),
        template = 'plotly_dark'
        )
    
    fig3 = go.Figure(data=data2, layout=layout2)
    return fig3

  

@app.callback(Output('test_txt', 'children'),
              [Input('metric-regions', 'value')])

def update_test_text(metric_regions_name):
    nb_test = df_santepub_dep.loc[df_santepub_dep['region_name'] == metric_regions_name]
    nb_test = nb_test['nb_test'].sum()
   
    return nb_test


   
@app.callback(Output('testpos_txt', 'children'),
              [Input('metric-regions', 'value')])

def update_testpos_text(metric_regions_name):
    nb_pos = df_santepub_dep.loc[df_santepub_dep['region_name'] == metric_regions_name]
    nb_pos = nb_pos['nb_pos'].sum()
   
    return nb_pos 

   
# lancer le dashboard
if __name__ == '__main__':
    app.run_server(debug=False)